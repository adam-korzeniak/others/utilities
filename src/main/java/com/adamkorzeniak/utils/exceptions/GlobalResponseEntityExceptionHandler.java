package com.adamkorzeniak.utils.exceptions;

import com.adamkorzeniak.utils.exceptions.model.ErrorResponse;
import com.adamkorzeniak.utils.exceptions.model.exception.BusinessException;
import com.adamkorzeniak.utils.exceptions.model.exception.ErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException exc, HttpHeaders headers, HttpStatus status, WebRequest request) {
        UUID uuid = UUID.randomUUID();
        log.error("Error occurred. UUID: {}", uuid, exc);
        Set<HttpMethod> supportedMethods = exc.getSupportedHttpMethods();
        if (!CollectionUtils.isEmpty(supportedMethods)) {
            headers.setAllow(supportedMethods);
        }
        var error = ErrorResponse.Error.of(ErrorType.METHOD_NOT_ALLOWED, exc.getMessage());
        ErrorResponse errorResponse = ErrorResponse.ofError(uuid, error);
        return new ResponseEntity<>(errorResponse, headers, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(
            BindException exc, HttpHeaders headers, HttpStatus status, WebRequest request) {
        UUID uuid = UUID.randomUUID();
        log.error("Error occurred. UUID: {}", uuid, exc);
        var errors = exc.getAllErrors().stream()
                .map(field -> ErrorResponse.Error.of(ErrorType.BAD_REQUEST, field.toString()))
                .collect(Collectors.toList());
        ErrorResponse errorResponse = ErrorResponse.ofErrors(uuid, errors);
        return new ResponseEntity<>(errorResponse, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BusinessException.class)
    protected ResponseEntity<ErrorResponse> businessException(BusinessException exc) {
        UUID uuid = UUID.randomUUID();
        log.error("Error occurred. UUID: {}", uuid, exc);
        var error = ErrorResponse.Error.of(exc.getCode(), exc.getTitle(), exc.getMessage());
        ErrorResponse errorResponse = ErrorResponse.ofError(uuid, error);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    protected ResponseEntity<ErrorResponse> accessDeniedException(AccessDeniedException exc) {
        UUID uuid = UUID.randomUUID();
        log.error("Error occurred. UUID: {}", uuid, exc);
        var error = ErrorResponse.Error.of("ACCESS DENIED", "Access Denied", exc.getMessage());
        ErrorResponse errorResponse = ErrorResponse.ofError(uuid, error);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = Throwable.class)
    protected ResponseEntity<ErrorResponse> defaultException(Throwable exc) {
        UUID uuid = UUID.randomUUID();
        log.error("Error occurred. UUID: {}", uuid, exc);
        String exceptionMessage = Optional.ofNullable(exc.getCause())
                .map(Throwable::getMessage)
                .orElse(exc.getMessage());
        var error = ErrorResponse.Error.of(ErrorType.INTERNAL_SERVER_ERROR, exceptionMessage);
        ErrorResponse errorResponse = ErrorResponse.ofError(uuid, error);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

