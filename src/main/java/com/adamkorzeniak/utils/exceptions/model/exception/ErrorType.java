package com.adamkorzeniak.utils.exceptions.model.exception;

import com.adamkorzeniak.utils.exceptions.model.ErrorInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorType implements ErrorInfo {
    BAD_REQUEST("HTTP_400", "Bad Request"),
    METHOD_NOT_ALLOWED("HTTP_405", "HTTP Method Not Allowed"),
    INTERNAL_SERVER_ERROR("ERROR", "Unexpected error occurred");

    private final String code;
    private final String title;
}
