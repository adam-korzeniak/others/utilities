package com.adamkorzeniak.utils.exceptions.model;

import com.adamkorzeniak.utils.exceptions.model.exception.ErrorType;
import lombok.*;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorResponse {

    private final Long timestamp = Instant.now().toEpochMilli();
    private final UUID errorId;
    private final List<Error> errors;

    public static ErrorResponse ofError(UUID uuid, Error error) {
        return new ErrorResponse(uuid, Collections.singletonList(error));
    }

    public static ErrorResponse ofErrors(UUID uuid, List<Error> errors) {
        return new ErrorResponse(uuid, errors);
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Error {
        private final String code;
        private final String title;
        private final String message;

        public static Error of(String code, String title, String message) {
            return new Error(code, title, message);
        }

        public static Error of(ErrorType errorType, String message) {
            return of(errorType.getCode(), errorType.getTitle(), message);
        }
    }


}
