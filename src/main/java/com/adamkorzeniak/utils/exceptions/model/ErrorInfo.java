package com.adamkorzeniak.utils.exceptions.model;

public interface ErrorInfo {

    String getCode();

    String getTitle();
}
