package com.adamkorzeniak.utils.exceptions.test.utils;

public class Urls {
    public static final String TEST_PATH = "/test";
    public static final String EXCEPTION_PATH = "/exception";
    public static final String BUSINESS_EXCEPTION_PATH = "/business-exception";
}
