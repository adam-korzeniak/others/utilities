package com.adamkorzeniak.utils.exceptions.test.utils;

public class Messages {
    public static final String INTERNAL_SERVER_ERROR_CODE = "ERROR";
    public static final String INTERNAL_SERVER_ERROR_TITLE = "Unexpected error occurred";
    public static final String UNEXPECTED_EXCEPTION_DUMMY_MESSAGE = "Unexpected error occurred";

    public static final String METHOD_NOT_ALLOWED_CODE = "HTTP_405";
    public static final String METHOD_NOT_ALLOWED_TITLE = "HTTP Method Not Allowed";
    public static final String PUT_METHOD_NOT_ALLOWED_MESSAGE = "Request method 'PUT' not supported";

    public static final String BAD_REQUEST_CODE = "HTTP_400";
    public static final String BAD_REQUEST_TITLE = "Bad Request";

    public static final String BUSINESS_EXCEPTION_CODE = "BUSINESS_CODE";
    public static final String BUSINESS_EXCEPTION_TITLE = "Business Exception Occurred";
    public static final String BUSINESS_EXCEPTION_MESSAGE = "Business Exception Occurred with message";

    public static final String EMPTY_RESPONSE = "";
    public static final String EMPTY_JSON_RESPONSE = "{}";
}
