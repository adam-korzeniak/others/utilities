package com.adamkorzeniak.utils.exceptions;

import com.adamkorzeniak.utils.exceptions.api.rest.DummyTestController;
import com.adamkorzeniak.utils.exceptions.test.utils.Messages;
import com.adamkorzeniak.utils.exceptions.test.utils.TestData;
import com.adamkorzeniak.utils.exceptions.test.utils.Urls;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@EnableWebMvc
@EnableConfigurationProperties
@AutoConfigureMockMvc
@SpringBootTest(classes = {DummyTestController.class, GlobalResponseEntityExceptionHandler.class})
class GlobalResponseEntityExceptionHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void GetEntity_NoErrors_OkResponseReturned() throws Exception {
        mockMvc.perform(get(Urls.TEST_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string(Messages.EMPTY_RESPONSE));
    }

    @Test
    void CreateEntity_BodyInvalid_BadRequestReturned() throws Exception {
        mockMvc.perform(post(Urls.TEST_PATH).contentType(MediaType.APPLICATION_JSON).content(Messages.EMPTY_JSON_RESPONSE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("errorId").exists())
                .andExpect(jsonPath("timestamp").exists())
                .andExpect(jsonPath("errors").exists())
                .andExpect(jsonPath("errors", hasSize(2)))
                .andExpect(jsonPath("errors[0].code", is(Messages.BAD_REQUEST_CODE)))
                .andExpect(jsonPath("errors[0].title", is(Messages.BAD_REQUEST_TITLE)))
                .andExpect(jsonPath("errors[0].message", containsString(TestData.DUMMY_PERSON_OBJECT_NAME)))
                .andExpect(jsonPath("errors[0].message", anyOf(containsString(TestData.NAME_FIELD), containsString(TestData.AGE_FIELD))))
                .andExpect(jsonPath("errors[1].code", is(Messages.BAD_REQUEST_CODE)))
                .andExpect(jsonPath("errors[1].title", is(Messages.BAD_REQUEST_TITLE)))
                .andExpect(jsonPath("errors[1].message", containsString(TestData.DUMMY_PERSON_OBJECT_NAME)))
                .andExpect(jsonPath("errors[1].message", anyOf(containsString(TestData.NAME_FIELD), containsString(TestData.AGE_FIELD))));
    }

    @Test
    void CreateEntity_InvalidMethod_MethodNotAllowedErrorReturned() throws Exception {
        mockMvc.perform(put(Urls.TEST_PATH))
                .andExpect(status().isMethodNotAllowed())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("errorId").exists())
                .andExpect(jsonPath("timestamp").exists())
                .andExpect(jsonPath("errors").exists())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].code", is(Messages.METHOD_NOT_ALLOWED_CODE)))
                .andExpect(jsonPath("errors[0].title", is(Messages.METHOD_NOT_ALLOWED_TITLE)))
                .andExpect(jsonPath("errors[0].message", is(Messages.PUT_METHOD_NOT_ALLOWED_MESSAGE)));
    }

    @Test
    void HttpRequest_BusinessException_BusinessErrorReturned() throws Exception {
        mockMvc.perform(get(Urls.BUSINESS_EXCEPTION_PATH))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.errorId").exists())
                .andExpect(jsonPath("timestamp").exists())
                .andExpect(jsonPath("errors").exists())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].code", is(Messages.BUSINESS_EXCEPTION_CODE)))
                .andExpect(jsonPath("errors[0].title", is(Messages.BUSINESS_EXCEPTION_TITLE)))
                .andExpect(jsonPath("errors[0].message", is(Messages.BUSINESS_EXCEPTION_MESSAGE)));
    }

    @Test
    void HttpRequest_UnexpectedException_InternalServerErrorReturned() throws Exception {
        mockMvc.perform(get(Urls.EXCEPTION_PATH))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.errorId").exists())
                .andExpect(jsonPath("timestamp").exists())
                .andExpect(jsonPath("errors").exists())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].code", is(Messages.INTERNAL_SERVER_ERROR_CODE)))
                .andExpect(jsonPath("errors[0].title", is(Messages.INTERNAL_SERVER_ERROR_TITLE)))
                .andExpect(jsonPath("errors[0].message", is(Messages.UNEXPECTED_EXCEPTION_DUMMY_MESSAGE)));
    }
}
